## 概述

操作者接口的设计灵感源自于 `MapStruct` 的 `Mapper` 接口。它提供了一种手动填充的补充方式，用于处理某些无法直接在类上配置注解的业务场景。

比如：

- 用于填充的对象是 `JSONObject` / `Map`，就没有对应的 Java 类，因此也无法在类上或类的属性上添加注解配置；
- 填充的对象的字段非常像，但是它们确实不是一个类，也不存在提取公共父类的可能，又不想要每个类都重复配置；

除了手动构建一个 `BeanOperations` 对象并调用执行器这种复杂的方法之外，还可以通过使用操作者接口来完成这类操作。

我们可以通过简单的注解配置将一个接口变为操作者，然后在其有参的抽象方法上通过 `@Assemble` 注解配置装配操作，此后，我们可以将任何对象或对象集合作为参数传入该方法，在调用后，将会按抽象方法上的操作配置去填充传入的对象或对象集合。

## 3.7.1.基本使用

首先，在一个**接口**上添加 `@Operator` 注解，将其声明为操作者。然后，在抽象方法上使用 `@Assemble` 注解配置装配操作，就像在类或类属性上进行配置一样。

比如：

```java
@Operator
private interface OperatorInterface {
    
    // 所有传入的 map 对象，都会根据 id 对应的值进行填充
    @Assemble(key = "id", container = "test", props = @Mapping(ref = "name"))
    void operate(Collection<Map<String, Object>> targets);
}
```

接下来，我们使用代理工厂 `OperatorProxyFactory` 生成操作者接口的代理类，并获得具备填充能力的代理对象：

```java
// 在 spring 环境可以跳过这一步，直接从 spring 容器获取相关组件

// 创建操作者接口的代理工厂
Crane4jGlobalConfiguration configuration = SimpleCrane4jGlobalConfiguration.create();
AnnotationFinder annotationFinder = new SimpleAnnotationFinder();
OperatorProxyFactory operatorProxyFactory = new OperatorProxyFactory(configuration, annotationFinder);
// 向代理工厂添加抽象方法适配器
operatorProxyFactory.addProxyMethodFactory(new DefaultOperatorProxyMethodFactory(configuration.getConverterManager()));
// 通过代理工厂创建代理对象
OperatorInterface operator = operatorProxyFactory.get(OperatorInterface.class);
```

在调用 `operate` 方法后，我们的输入参数 `targets` 将根据 `operate` 方法上的配置进行填充：

~~~java
// 调用抽象方法完成填充操作
Collection<Map<String, Object>> targets = IntStream.rangeClosed(0, 5).mapToObj(id -> {
    Map<String, Object> target = new HashMap<>();
    target.put("id", id);
    return target;
}).collect(Collectors.toList());
operator.operate(targets);
~~~

:::tip

- 所有 `AssembleXXX` 格式的操作配置注解都可以在方法上使用，因此操作者接口的配置方式与基于类或属性的方式基本相同；
- 配置解析器 `BeanOperationParser` 可以从任何带注解的 `AnnotatedElement` 解析并获取 `BeanOperations` 配置对象，不仅限于 `Class` 或 `Method`。操作者接口受益于此特性，你可以通过它来扩展更多玩法；

:::

## 3.7.2.在 Spring 中使用

类似于 MyBatis，你可以在 Spring 环境中的启动类或配置类上添加 `@OperatorScan` 注解，方便地批量扫描、添加或排除操作者接口：

```java
@OperatorScan(
    includePackages = {"cn.crane4j.example.operators", "cn.crane4j.spring.example.operators"},
    includeClasses = FooOperator.class, excludes = ExcludeOpeator.class
)
@Configuration
protected class Application {
}
```

在项目启动后，将自动为接口创建 `BeanDefinition`，并在 Spring 容器中创建对应的 Bean。因此，你可以像使用 MyBatis 的 Mapper 一样，通过依赖注入来使用操作者接口：

```java
@Component
public class FooService {
    @Autowired
    private FooOperator operator; // 注入操作者接口
}
```

## 3.7.3.动态数据源容器

通过向操作者接口的代理工厂 `OperatorProxyFactory` 注册 `DynamicContainerOperatorProxyMethodFactory` 方法适配器，我们可以令具有不止一个参数的抽象方法支持动态数据源功能：

~~~java
// 在 spring 环境可以跳过这一步，直接从 spring 容器获取相关组件

Crane4jGlobalConfiguration configuration = // 上文创建的全局配置对象
OperatorProxyFactory operatorProxyFactory = // 上文创建的代理工厂
OperatorProxyMethodFactory proxyMethodFactory = new DynamicContainerOperatorProxyMethodFactory(
    configuration.getConverterManager(), new SimpleParameterNameFinder(), new SimpleAnnotationFinder()
);
operatorProxyFactory.addProxyMethodFactory(proxyMethodFactory);
~~~

### 3.7.3.1.声明参数为容器

操作者接口在一次执行中可以使用临时容器来替换原有的数据源容器。在方法的参数中，除了第一个参数外，其他参数将被传入作为临时容器。

例如：

```java
@Operator
private interface OperatorInterface3 {
    @Assemble(key = "id", container = "test", props = @Mapping(ref = "name"))
    void operate(Collection<Map<String, Object>> targets, @ContainerParam("test") Map<Integer, ?> tempData);
}
```

在上面的示例中，我们将方法的第二个参数指定为临时数据源容器。这意味着，在执行操作时，会直接使用传入的 `Map` 对象作为名为 "test" 的数据源容器，而不是使用全局配置类中的容器。

当然，我们也可以不使用 `@ContainerParam` 注解，默认情况下，参数名将作为临时容器的命名空间。比如：

~~~java
// 写法等同于
// void operate(Collection<Map<String, Object>> targets, @ContainerParam("test") Map<Integer, ?> tempData);
@Assemble(key = "id", container = "test", props = @Mapping(ref = "name"))
void operate(Collection<Map<String, Object>> targets, Map<Integer, ?> test);
~~~

:::warning

在非 spring 环境中，需要用户自己开启编译保留参数名的选项，不然编译后参数名会变为 `arg0`、`arg1` 这种格式。

:::

### 3.7.3.2.使用参数容器

我们可以按照以下方式使用它：

```java
OperatorInterface operator = operatorProxyFactory.get(OperatorInterface.class);

// 声明一个待处理对象
Map<String, Object> target = new HashMap<>();
target.put("id", 1);
Collection<Map<String, Object>> targets = Collections.singletonList(target);

// source 将作为临时数据源
Map<Integer, Object> source = new HashMap<>();
source.put(1, "name1");

// 填充待处理对象，数据源容器 test 将会直接返回 source 作为本次填充的数据源对象 
operator.operate(targets, source);
Assert.assertEquals("name1", target.get("name"));
```

### 3.7.3.3.参数适配器

这个功能是基于 `DynamicContainerOperatorProxyMethodFactory` 实现的，默认支持以下三种类型的参数适配：

- `Map`：参数将被适配为 `ConstantContainer`；
- `Container`：直接使用；
- `DataProvider`：参数将被适配为 `LambdaContainer`；

如果有必要，用户还可以通过 `DynamicContainerOperatorProxyMethodFactory` 的 `addAdaptorProvider` 方法添加其他类型的参数适配器：

```java
DynamicContainerOperatorProxyMethodFactory factory = SpringUtil.getBean(DynamicContainerOperatorProxyMethodFactory.class);
// 将 LinkedHashMap 类型的参数适配为 ConstantContainer
factory.addAdaptorProvider(LinkedHashMap.class, (name, parameter) ->
	arg -> Containers.forMap(name, (Map<Object, ?>) arg)
));
```

## 3.7.4.指定执行器和解析器

类似于 `@AutoOperate`，`@Operator` 接口也可以指定用于执行操作的执行器和配置解析器。

比如：

~~~java
@Operator(
    executorType = OrderedBeanOperationExecutor.class,
    parserType = TypeHierarchyBeanOperationParser.class
)
private interface OperatorInterface {
    @Assemble(key = "id", container = "test", props = @Mapping(ref = "name"))
    void operate(Collection<Map<String, Object>> targets);
}
~~~

## 3.7.5.代理方法工厂

与 Spring 中将被 `@EventListener` 注解的方法适配为监听器的模式类似，操作者接口中抽象方法的解析也是基于策略模式。

操作者的代理方法工厂 `OperatorProxyMethodFactory` 默认提供了两种实现：

- `DefaultProxyMethodFactory`：默认的代理方法工厂，支持处理所有有参方法；
- `DynamicSourceProxyMethodFactory`：动态数据源方法工厂，用于支持有不止一个参数的方法；

接口中的一个抽象方法仅会使用最匹配的工厂去生成代理方法。因此若有必要，用户也可以自行实现接口并提高工厂的优先级以替换默认策略。

在 Spring 环境中，只需将自定义工厂类声明为 Spring Bean，即可自动注册。在非 Spring 环境中，用户需要在创建代理工厂 `OperatorProxyFactory` 时将所需的方法工厂作为参数传入。
