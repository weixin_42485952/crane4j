## 概述

`crane4j` 提供了分组填充的功能，类似于 `hibernate-validator` 中的分组校验。

分组填充功能允许您定义不同的填充组，然后在使用 `@Assemble` 或 `@Disassemble` 进行填充操作时，通过 `groups` 属性指定要使用的填充组。这样，只有与指定组匹配的属性才会被填充或拆卸，例如在创建对象时只填充必要的属性，而在更新对象时填充所有属性。

<img src="https://img.xiajibagao.top/image-20230225012401927.png" alt="image-20230225012401927" style="zoom:150%;" />

## 3.5.1.为操作分组

在 `crane4j` 中，您可以通过在 `@Assemble` 和 `@Disassemble` 注解中使用 `groups` 属性来指定分组。

比如：

~~~java
public class UserVO {
    @Assemble(container = "user", props = @Mapping(src = "role", ref = "role"), groups = "admin")
    @Assemble(container = "user", props = @Mapping(src = "name", ref = "name"), groups = {"base", "admin"})
    private Integer id;
    private String name;
    private String role;
}
~~~

在示例中，我们有一个 `UserVO` 类，其中有一个 `id` 属性，我们声明了两个装配操作：

- 第一个装配操作是根据 `id` 装配 `name` 属性，这个装配操作只在 `admin` 组中生效；
- 第二个装配操作是根据 `id` 装配 `role` 属性，这个装配操作在 `base` 和 `admin` 组中生效；

当我们执行填充操作时，如果指定的组中包含了装配操作的分组，那么该装配操作将生效，否则，它将被忽略。

类似的，`@Disassemble` 注解也支持使用 `groups` 属性。

比如：

~~~java
public class Foo {
    @Assemble(container = "user", props = @Mapping(src = "name", ref = "name"), groups = "admin")
    private Integer id;
    private String name;
    @Disassemble(type = Foo.class, groups = "nested")
    private List<Foo> fooList;
}
~~~

在上述示例中，仅当指定执行操作组为 `nested` 时，才会执行对 `fooList` 的拆卸操作。

## 3.5.2.按分组规则执行操作

在 `crane4j` 中，您可以在手动填充和自动填充的情况下使用分组功能。

对于手动填充，您可以使用 `OperateTemplate` 的不同重载方法来设置本次填充操作的执行组：

~~~java
// 如果目标对象所属的填充组与指定的任何一个组匹配，执行填充操作
executeIfMatchAnyGroups(Object target, String... groups);

// 如果目标对象所属的填充组与指定的任何一个组都不匹配，执行填充操作
executeIfNoneMatchAnyGroups(Object target, String... groups);

// 如果目标对象所属的填充组与指定的所有组都匹配，执行填充操作
executeIfMatchAllGroups(Object target, String... groups);

// 仅执行通过指定过滤器条件的操作
execute(Object target, Predicate<? super KeyTriggerOperation> filter);
~~~

当自动填充时，您可以在 `@AutoOperate` 注解中使用 `includes` 属性来指定要执行填充操作的组：

~~~java
// 填充返回值
@AutoOperate(type = Foo.class, includes = "base")
public List<Foo> getFooList() {
    // do nothing
}

// 填充参数
@ArgAutoOperate(
    @AutoOperate(value = "foos", type = Foo.class, includes = "base")
)
public void setFooList(List<Foo> foos) {
    // do nothing
}
~~~
