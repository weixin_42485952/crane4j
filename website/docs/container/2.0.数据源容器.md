## 概述

在编程世界中，万事万物皆对象。从某种程度上说，无论是枚举、可调用方法，甚至是简单的 Map 集合，任何能够返回所需数据源对象的实体都可以被视为“盛放”数据的“容器”。

基于这个理念，`crane4j` 认为 **每个数据源都等同于一个容器，即数据源容器 `Container`**。而每个容器都对应一个独一无二的 `namespace`，作为它们的唯一标识。

<img src="https://img.xiajibagao.top/image-20230210133633050.png" alt="image-20230210133633050" style="zoom: 33%;" />

## 2.0.1.默认实现

在代码层面上，容器是指实现了 `Container` 接口的类。`crane4j` 提供了四种容器实现，以覆盖大部分业务场景：

- `ConstantContainer`：基于内存的常量容器，可用于支持字典项常量、枚举、常量类等数据源；
- `MethodInvokerContainer`：基于方法的容器，可以将实例方法或静态方法作为数据源；
- `LambdaContainer`：基于 lambda 表达式的容器，支持将任意输入 key 值并返回 Map 集合的 lambda 表达式适配为数据源，它是前述两者的补充；
- `EmptyContainer`：特殊的空容器，用于占位并表示数据源就是待处理对象本身，主要用于内省（即自己与自己本身进行属性映射）的场景；

用户还可以自行实现 `Container` 接口，以创建自定义的容器。

## 2.0.2.注册容器

所有的数据源容器可以在拿到全局配置类 `Crane4jGlobalConfiguration` 后进行手动注册：

~~~java
// 从 spring 容器中获取 Crane4jApplicationContext 实例
Crane4jGlobalConfiguration configuration = SpringUtils.getBean(Crane4jGlobalConfiguration.class);
// 创建一个常量容器，namespace 为 gender
Map<String, Object> gender = new HashMap<>();
gender.put("0", "女");
gender.put("1", "男");
Container<String> container = Containers.forMap("gender", gender);
// 注册容器
configuration.registerContainer(container);
~~~
