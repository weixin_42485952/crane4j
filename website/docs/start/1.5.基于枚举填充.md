## 1.5.1.基本使用

在 2.1 及以上版本，你可以基于 `@AssembleEnum` 注解快速声明一个基于枚举的填充操作：

~~~java 
@RequiredArgsConstructor
@Data
private static class Foo {
    @AssembleEnum(
        type = Gender.class,  // 指定枚举类型为 Gender
        enumKey = "code", // 根据枚举的 code 属性找到枚举
        props = @Mapping(src = "value", ref = "genderName") // 将枚举的 value 属性映射到 genderName 上
    )
    private final Integer genderCode;
    private String genderName;
}

@Getter
@RequiredArgsConstructor
private enum Gender {
    FEMALE(0, "女"), 
    MALE(1, "男");
    private final Integer code;
    private final String value;
}
~~~

在上述代码中，我们配置了这样一个操作：

+ 指定数据源为 `Gender` 枚举；
+ 填充时，找到 `code` 字段值与 `genderCode` 的字段值相同的枚举实例；
+ 将枚举实例中的 `value` 属性映射到 `genderName` 上；

**通过类全限定名指定类型**

若不方便直接指定枚举类型，你也可以使用枚举类的全限定名：

~~~java
@AssembleEnum(
    typeName = "com.example.enums.Gender",  // 通过全限定名指定枚举类型为 Gender
    enumKey = "code", // 根据枚举的 code 属性找到枚举
    props = @Mapping(src = "value", ref = "genderName") // 将枚举的 value 属性映射到 genderName 上
)
private final Integer genderCode;
~~~

## 1.5.2.简化配置

考虑到一般使用场景中，枚举并不会有太多需要引用的属性，因此在这种情况下，我们可以简化写法：

~~~java
@AssembleEnum(
    typeName = "com.example.enums.Gender",  // 通过全限定名指定枚举类型为 Gender
    enumKey = "code", enumValue = "value", // 指定枚举的 key 与 value
    ref = "genderName" // 直接将 enumValue 对应的字段值映射到 genderName 属性
)
private final Integer genderCode;
~~~

即省略 `props` 配置，直接通过 `ref` 指定属性映射。

或者，我们可以结合枚举类上的注解 `@ContainerEnum` 进一步的简化配置：

~~~java
@RequiredArgsConstructor
@Data
private static class Foo {
    @AssembleEnum(type = Gender.class, ref = "genderName")
    private final Integer genderCode;
    private String genderName;
}

@ContainerEnum(key = "code", value = "value") // 总是根据 code 寻找 value
@Getter
@RequiredArgsConstructor
private enum Gender {
    FEMALE(0, "女"), 
    MALE(1, "男");
    private final Integer code;
    private final String value;
}
~~~

:::tip

除此之外，你也可以直接在注解上添加 `@ContainerEnum` 注解将其作为独立数据源，然后按传统方式使用 `@Assemble` 注解配置填充操作。

关于 `@ContainerEnum` 注解，具体请参见后文“数据源容器” 一章中的 “枚举” 一节。

:::

## 1.5.3.作为独立数据源使用

除了直接使用 `@AssembleEnum` 外，你也可以按传统的方式配置，即先将枚举类转为容器并注册到全局配置类，然后再使用 `@Assemble` 将其作为一个普通的数据源容器使用。

**配置枚举数据源容器**

首先，你需要为枚举类添加 `@ContainerEnum` 注解，将其声明为数据源容器：

~~~java
@ContainerEnum(namespace = "gender", key = "code", value = "value") // 总是根据 code 寻找 value
@Getter
@RequiredArgsConstructor
private enum Gender {
    FEMALE(0, "女"), 
    MALE(1, "男");
    private final Integer code;
    private final String value;
}
~~~

**注册数据源容器**

然后像前文“配置数据源”一节中那样，将其注册到全局配置类中：

~~~java
Container<String> container = Containers.forEnum(Num.class, new SimpleAnnotationFinder());
crane4jGlobalConfiguration.registerContainer(container);
~~~

或者，你也可以像 MybatisPlus 那样，配置一个路径让 crane4j 自动扫描带有 `@ContainerEnum` 的枚举类：

~~~java
@EnableCrane4j(
    enumPackages = "com.example.demo" // 要扫描的枚举包路径
)
@SpringBootApplication
public class Demo3Application implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(Demo3Application.class, args);
    }
}
~~~

**使用**

最后，按传统的方式使用：

~~~java
@RequiredArgsConstructor // 使用 lombk 生成构造器
public class Foo {
    @Assemble(container = "gender", props = @Mapping(ref = "genderName"))
    private final Integer genderCode;
    private String genderName;
}
~~~

